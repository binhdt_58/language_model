/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package language_modeling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
/**
 *
 * @author binhdang
 */
public class probOfSequences {
    HashMap<String, Integer> _1gramdict = new HashMap<String, Integer>();
    HashMap<String, Integer> _2gramdict = new HashMap<String, Integer>();
    HashMap<String, Integer> _3gramdict = new HashMap<String, Integer>();
    int N;
    public void loadNgramModel( String encoded1gramFile, String encoded2gramFile,
            String encoded3gramFile){
        load_model lm = new load_model();
        
        System.out.println("loading 1-gram model");
        this._1gramdict = lm.load_ngram(1,encoded1gramFile);
        System.out.println("loading 2-gram model");
        this._2gramdict = lm.load_ngram(2,encoded2gramFile);
        System.out.println("loading 3-gram model");
        this._3gramdict = lm.load_ngram(3,encoded3gramFile);
        N = lm.getN();
    }
    
    public double compute1gram(String word){
        
        int freq1;
        if(this._1gramdict.containsKey(word))
            freq1 = this._1gramdict.get(word);
        else
            freq1 = 0;
        //System.out.println(freq);
        return laplaceSmothing(freq1,this.N);
    } 
    
    public double compute2gram (String word1, String word2){
        int freq1;
        int freq2;
        if (this._2gramdict.containsKey(word1+word2))
            freq1 = this._2gramdict.get(word1+word2);
        else
            freq1 = 0;
        
        if(this._1gramdict.containsKey(word1))
            freq2 = this._1gramdict.get(word1);
        else
            freq2 = 0;
        
        return laplaceSmothing(freq1,freq2);
    }
    
    public double compute3gram (String word1, String word2, String word3){
        int freq1;
        int freq2;
        if(this._3gramdict.containsKey(word1+word2+ word3))
            freq1 = this._3gramdict.get(word1+word2+ word3);
        else
            freq1 = 0;
        if (this._2gramdict.containsKey(word1+word2))
            freq2 = this._2gramdict.get(word1+word2);
        else
            freq2 = 0;
        return laplaceSmothing(freq1,freq2);
    }
    
    
    /*public double compute_ngram (int n){
        int freq1;
        int freq2;
        
        return laplaceSmothing(freq1,freq2);
    }*/
    public double laplaceSmothing(int numerator, int denominator){
        return -Math.log((numerator + 0.1)*1.0/(denominator + this._1gramdict.size()*0.1));
    }
    
    public double computeSenquence(int n, String sentence){
        double prob = 1;
        double perplexity = 0;
        sentence = "<s> <s> " + sentence + " </s> </s>";
        String[] tokens = sentence.split(" ");
        for(int i=0; i<tokens.length-3; i++){
            double x1=1,x2=1,x3=1, probTri = 0;
            double prob1= this.compute1gram(tokens[i+2]);
            double prob2= this.compute2gram(tokens[i+1], tokens[i+2]);
            double prob3= this.compute3gram(tokens[i], tokens[i+1], tokens[i+2]);
            probTri = x1*prob3+x2*prob2+x3*prob1;
            prob = prob*probTri;  
            perplexity += Math.log(probTri);
            
            
        }
        double entro = /*-(1/tokens.length)*/perplexity ;
        
        if(n==0)
            return prob;
        else
            
            return entro;
    }
    
    public ArrayList<Double> computeFile(int n,String testFile){
        double prep =1.0;
        int m=0;
        ArrayList<String> lines = new ArrayList<String>();
        ArrayList<Double> prob = new ArrayList<Double>();
        try{
            BufferedReader br = new BufferedReader(new FileReader(testFile));
            String s;
            while((s=br.readLine()) != null){
                lines.add(s);
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
        for (String line : lines) {
            String[] tokens = line.split(" ");
            m = m+tokens.length;
            prob.add(this.computeSenquence(n,line)) ;
            //System.out.println(m);
        }
 
        if(n==0){
            
            return prob;
        }else{
            for (double p :prob){
                
                prep = prep+p;
                //System.out.println(prep);
            }
            
            
            System.out.println(Math.pow(2,-prep/m));
            return null;
            
        }
        
        
    }
}
