/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package language_modeling;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author binhdang
 */
public class load_model {
    private int N;
    public load_model(){
        
    }
    
    public int getN(){return this.N;}
    
    public ArrayList<String> getlines(String infile){
        ArrayList<String> lines = new ArrayList<String>();
        try{
            BufferedReader br = new BufferedReader(new FileReader(infile));
            String s;
            while((s=br.readLine()) != null){
                lines.add(s);
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }
        return lines;
    }
    
   /* public HashMap<String, Integer> load1gram(String encoded1gramFile){
        this.N = 0;
        HashMap<String, Integer> Stringdict1 = new HashMap<String, Integer>();
        ArrayList<String> lines = this.getlines(encoded1gramFile);
        for(int i=0; i<lines.size(); i++){
            String[] tokens = lines.get(i).split(" ");
            String unigram = tokens[0];
            Stringdict1.put(unigram, Integer.parseInt(tokens[1]));
            N += Integer.parseInt(tokens[1]);
        }
        return Stringdict1;
    }
    
    public HashMap<String, Integer> load2gram(String encoded2gramFile){
        HashMap<String, Integer> Stringdict2 = new HashMap<String, Integer>();
        ArrayList<String> lines = this.getlines(encoded2gramFile);
        for(int i=0; i<lines.size(); i++){
            String[] tokens = lines.get(i).split(" ");
            Stringdict2.put(tokens[0]+tokens[1]+"", Integer.parseInt(tokens[2]));
        }
        return Stringdict2;
    }
    
    public HashMap<String, Integer> load3gram(String encoded3gramFile){
        HashMap<String, Integer> Stringdict3 = new HashMap<String, Integer>();
        ArrayList<String> lines = this.getlines(encoded3gramFile);
        for(int i=0; i<lines.size(); i++){
            String[] tokens = lines.get(i).split(" ");
            
            Stringdict3.put((tokens[0]+tokens[1]+tokens[2]), Integer.parseInt(tokens[3]));
        }
        return Stringdict3;
    }
    */
    public HashMap<String, Integer> load_ngram(int n,String encoded3gramFile){
        this.N =0;
        HashMap<String, Integer> Stringdictn = new HashMap<String, Integer>();
        ArrayList<String> lines = this.getlines(encoded3gramFile);
        for(int i=0; i<lines.size(); i++){
            String[] tokens = lines.get(i).split(" ");
            String s = null;
            for(int j=0; j<n; j++){
                s += tokens[j];
            }
            if(n==1){
                this.N +=Integer.parseInt(tokens[n]);
            }
            Stringdictn.put(s, Integer.parseInt(tokens[n]));
        }
        return Stringdictn;
    }
}
