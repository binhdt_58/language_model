/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package language_modeling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
/**
 *
 * @author binhdang
 */
public class counting {
    private ArrayList<String> rawData = new ArrayList<String>();
    public counting(String trainingFile) {
        try{
            BufferedReader reader = new BufferedReader(new FileReader(trainingFile));
            String s = "";
            while((s=reader.readLine())!= null){
                this.rawData.add(s.toLowerCase());
            }
            reader.close();
        }catch(FileNotFoundException ex){
            System.out.println("File not found!");
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }
    
    /*public void count1gram(String outFile){
        HashMap<String, Integer> _1gram = new HashMap<String, Integer>();
        System.out.print("Counting 1-gram.....");
        for(int i=0; i<this.rawData.size(); i++){
            String[] tokens = this.rawData.get(i).split(" ");
            for(int j=0; j<tokens.length; j++){
                
                if(!_1gram.containsKey(tokens[j]))
                    _1gram.put(tokens[j], 1);
                else{
                    int count = _1gram.get(tokens[j]) + 1;
                    _1gram.put(tokens[j], count);
                }
            }
        }
        try{
            BufferedWriter wr = new BufferedWriter(new FileWriter(outFile));
            Iterator iterator = _1gram.keySet().iterator();
            while(iterator.hasNext()){
                String str = (String) iterator.next();
                wr.append(str + " " + _1gram.get(str) + "\n");
            }
            wr.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
        System.out.println("Ok!");
    }
    
    public void count2gram(String outFile){
        HashMap<String, Integer> _2gram = new HashMap<String, Integer>();
        System.out.print("Counting 2gram.....");
        for(int i=0; i<this.rawData.size(); i++){
            String[] tokens = this.rawData.get(i).split(" ");
            for(int j=0; j<tokens.length-1; j++){
                String s = tokens[j] + " " + tokens[j+1];
                if(!_2gram.containsKey(s))
                    _2gram.put(s, 1);
                else{
                    int count = _2gram.get(s) + 1;
                    _2gram.put(s, count);
                }
            }
        }
        try{
            BufferedWriter wr = new BufferedWriter(new FileWriter(outFile));
            Iterator iterator = _2gram.keySet().iterator();
            while(iterator.hasNext()){
                String str = (String) iterator.next();
                wr.append(str + " " + _2gram.get(str) + "\n");
            }
            wr.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
        System.out.println("Ok!");
    }
    
    public void count3gram(String outFile){
        HashMap<String, Integer> _3gram = new HashMap<String, Integer>();
        System.out.print("Counting 3gram.....");
        for(int i=0; i<this.rawData.size(); i++){
            String[] tokens = this.rawData.get(i).split(" ");
            for(int j=0; j<tokens.length-2; j++){
                String s = tokens[j] + " " + tokens[j+1] + " " + tokens[j+2];
                if(!_3gram.containsKey(s))
                    _3gram.put(s, 1);
                else{
                    int count = _3gram.get(s) + 1;
                    _3gram.put(s, count);
                }
            }
        }
        try{
            BufferedWriter wr = new BufferedWriter(new FileWriter(outFile));
            Iterator iterator = _3gram.keySet().iterator();
            while(iterator.hasNext()){
                String str = (String) iterator.next();
                wr.append(str + " " + _3gram.get(str) + "\n");
            }
            wr.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
        System.out.println("Ok!");
    }*/
    
    
    public void count_ngram(int n ,String outFile){
        HashMap<String, Integer> _ngram = new HashMap<String, Integer>();
        System.out.print("Counting "+ n +"gram.....");
        for(int i=0; i<this.rawData.size(); i++){
            String[] tokens = this.rawData.get(i).split(" ");
            for(int j=0; j<tokens.length-n+1; j++){
                String s = null;
                for (int k=0; k<n; k++){
                    s += tokens[j+k] + " " ;
                }
                
                if(!_ngram.containsKey(s))
                    _ngram.put(s, 1);
                else{
                    int count = _ngram.get(s) + 1;
                    _ngram.put(s, count);
                }
            }
        }
        try{
            BufferedWriter wr = new BufferedWriter(new FileWriter(outFile));
            Iterator iterator = _ngram.keySet().iterator();
            while(iterator.hasNext()){
                String str = (String) iterator.next();
                wr.append(str+ _ngram.get(str) + "\n");
            }
            wr.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
        System.out.println("Ok!");
    }
    
}
